<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Comment;
use App\Http\Requests\CommentRequest;

class CommentController extends Controller
{
      public function create(Post $post, CommentRequest $request)
    {
        $comment = new Comment();
        $comment->comment = $request->input('comment');
        $comment->user_id = $request->user()->id;
        $comment->post_id = $post->id;
        $comment->save();
        return redirect()->route('item_post', $comment->post_id);
    }
    public function destroy(Comment $comment, User $user)
    {
        $this->authorize('delete', $comment, $user);
        $comment->delete();
        return redirect()->route('item_post', $comment->post->id);
    }
}
