<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use App\Http\Requests\PostRequest;

class PostController extends Controller
{
    public function list()
    {
        return view('post.list', [
            'list' =>  Post::paginate(5)
        ]);
    }

    public function create()
    {
        return view('post.create');
    }


    public function created(PostRequest $request)
    {
        $post = new Post();
        $post->title = $request->input('title');
        $post->article = $request->input('article');
        $post->user_id = $request->user()->id;
        $post->save();
        return redirect()->route('item_post', $post->id);
    }

    public function show(Post $post)
    {
        return view('post.show', [
            'post' => $post,
            'comment_list' => $post->comments()->paginate(5)
        ]);
    }

    public function edit(Post $post, User $user)
    {
        $this->authorize('update', $post, $user);
        return view('post.edit', [
            'item' => $post,
        ]);
    }

    public function edited(Post $post, User $user, PostRequest $req)
    {
        $this->authorize('update', $post, $user);
        $post->title = $req->input('title');
        $post->article = $req->input('article');

        $post->save();

        return redirect()->route('item_post', $post->id);
    }
    public function destroy(Post $post, User $user)
    {
        $this->authorize('delete', $post, $user);
        $post->delete();
        return redirect()->route('blog');
    }
}

