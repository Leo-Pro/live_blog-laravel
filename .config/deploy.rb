lock "~> 3.11.1"

set :application, "leopro-laravel-blog"
set :repo_url, "git@bitbucket.org:Leo-Pro/live_blog-laravel.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, ->{ "/home/deploy/#{fetch :application}" }

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
set :format_options, command_output: true, log_file: "/dev/null", color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
append :linked_files, *%w( .env )

# Default value for linked_dirs is []
append :linked_dirs, *%w( storage/app/public  )

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
set :keep_releases, 10

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure

namespace :deploy do

  desc 'Install application modules, compile bundle, etc'
  task :bundle do
    on release_roles(:web) do
      within release_path do
        execute :composer, 'install --prefer-dist --no-ansi -n --no-progress --no-suggest --optimize-autoloader'
        execute :npm, 'install'
        execute :npm, 'run production'
      end
    end
  end

  desc 'Bootstrap web application'
  task :bootstrap do
    on release_roles(:web) do
      within release_path do
        execute  :php, 'artisan migrate --force'
        execute :php, 'artisan storage:link -q'
        execute :chmod, '-R g+ws storage bootstrap/cache'
      end
    end
  end
end

# steps
after "deploy:updated", "deploy:bundle"
after "deploy:bundle", "deploy:bootstrap"
