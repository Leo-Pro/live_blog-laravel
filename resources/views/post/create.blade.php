@extends('layouts.app')
@section('title-block')Создать@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

        <form class="card" action="{{ route('create_post') }}" method="post">
            @csrf
                <div class="row-form-group">
                    <label for="title">Заголовок</label>
                    <input class="form-control card-header" type="text" name="title" id="title">
                </div>

                <div class="form-group">
                    <label for="article">Статья</label>
                    <textarea class="form-control  card-body" type="text"  name="article" id="article" cols="30" rows="10"></textarea>
                </div>
                <div class="btn-group">
                     <input class="btn btn-primary"type="submit" value="Сохранить">
                    <input class="btn btn-link"type="reset" value="Сбросить">
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
