@extends('layouts.app')
@section('title-block')Блог@endsection

@section('content')
    <div class="col-md-12">
        <div class="col-md-12 page-body">
            <div class="row">


                <div class="sub-title">
                    <h2>Live Blog</h2>
                    @can('create', App\Models\Post::class)
                    <a href="{{ route('new_post') }}" >Создать пост</a>
                    @endcan
                </div>


                <div class="col-md-12 content-page">
                    <!-- Blog Post Start -->
                    @foreach ($list as $item)
                    <div class="col-md-12 blog-post">
                        <div class="post-title">
                            <a href="{{ route('item_post', $item->id) }}"> <h1>{{ $item->title }}</h1></a>
                        </div>
                        <div class="post-info">
                            <span>{{ $item->created_at->format('d M y') }} / by <a href="#" target="_blank">{{ $item->user->name }}</a></span>
                        </div>
                        <p></p>
                        <a href="{{ route('item_post', $item->id) }}" class="button button-style button-anim fa fa-long-arrow-right"><span>Читать полностью</span></a>
                    </div>
                    @endforeach
                    <!-- Blog Post End -->


                    {{ $list->links() }}

                </div>

            </div>



        </div>




    </div>
@endsection
