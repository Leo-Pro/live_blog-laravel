@extends('layouts.app')
@section('title-block'){{ $post->title }}@endsection

@section('content')
    <!-- Blog Post (Right Sidebar) Start -->
    <div class="col-md-12">
        <div class="col-md-12 page-body">
            <div class="row">


                <div class="sub-title">
                    <h2>Live Blog</h2>
                    <a href="{{ route('blog') }}">назад</a>
                </div>


                <div class="col-md-12 content-page">


                    <!-- Blog Post Start -->
                    <div class="col-md-12 blog-post">
                        <div class="post-title">
                            <h1>{{ $post->title }}</h1>
                        </div>
                        <div class="post-info">
                            <span>{{ $post->created_at->format('d M y') }} / by <a target="_blank">{{ $post->user->name }}</a></span>
                        </div>
                        <p>{{ $post->article }}</p>
                        @can('delete', $post)
                        <a href="{{ route('edit_post', $post->id) }}" class="button button-style button-anim fa fa-long-arrow-left"><span>редактировать</span></a>
                        <form method="POST" action="{{ route('delete_post', $post->id) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="button button-style button-anim fa fa-long-arrow-right">удалить</button>
                        </form>
                        @endcan
                    </div>
                    <!-- Blog Post End -->

                    @include('post.comment.create')
                    @include('post.comment.list')
                </div>
            </div>
        </div>

    </div>
    <!-- Blog Post (Right Sidebar) End -->
@endsection
