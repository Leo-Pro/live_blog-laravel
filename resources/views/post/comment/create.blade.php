@can('create', App\Models\Post::class)
<form class="card" action="{{ route('create_comment', $post) }}" method="POST">
@csrf
    <textarea name="comment" id="comment" cols="30" rows="10"></textarea>
    <button type="submit" class="btn btn-success">Добавить комментарий</button>
</form>
@endcan
