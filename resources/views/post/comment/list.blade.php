
<div class="card">
    @foreach($comment_list as $comment)
    <div class="card-header">{{ $comment->user->name }}, {{ $comment->created_at }}</div>
    <div class="card-body">
        <p>{{ $comment->comment }}</p>
    </div>

        @can('delete', $comment)
            <form method="POST" action="{{ route('delete_comment', $comment->id) }}">
                @csrf
                @method('DELETE')
                <button type="submit" class="button button-style button-anim fa fa-long-arrow-right">удалить</button>
            </form>
        @endcan

    @endforeach
    <hr>
     {{ $comment_list->links() }}
</div>
