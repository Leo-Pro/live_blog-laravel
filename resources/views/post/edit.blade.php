@extends('layouts.app')
@section('title-block')Редактирование@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="card" method="post" action="{{ route('edit_post_save', $item->id) }}">
                @csrf
                @method('PUT')
                <div class="row-form-group">
                    <label for="title">Заголовок</label>
                    <input class="form-control card-header" name="title" id="title" value="{{ $item->title }}">
                </div>
                <div class="form-group">
                    <label for="article">Статья</label>
                    <textarea class="form-control card-body" name="article" id="article" cols="30" rows="10">{{ $item->article }}</textarea>
                </div>
                <input type="submit" value="Сохранить" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
@endsection
