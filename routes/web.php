<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'PostController@list')->name('blog');

Route::get('/posts', 'PostController@create')->name('new_post');
Route::post('/posts', 'PostController@created')->name('create_post');

route::get('/posts/{post}', 'PostController@show')->name('item_post');
Route::get('/posts/{post}/edit', 'PostController@edit')->name('edit_post');

Route::put('/posts/{post}', 'PostController@edited')->name('edit_post_save');
Route::delete('/posts/{post}', 'PostController@destroy')->name('delete_post');

Route::post('/posts/{post}/comments', 'CommentController@create')->name('create_comment');
Route::delete('/posts/comments/{comment}', 'CommentController@destroy')->name('delete_comment');
